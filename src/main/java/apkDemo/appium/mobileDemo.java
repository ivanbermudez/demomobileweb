package apkDemo.appium;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import io.appium.java_client.MobileElement;

public class mobileDemo {
	UtilsMobile ut;
	PrintWriter out;
	PrintWriter out2;
	
	public mobileDemo(String fecha) {
	
        ut=new UtilsMobile(fecha);
	}
	
	 public void revisarPaginaMobile() {
	    	
    	 try {
 			out = new PrintWriter("src/main/java/recursos/domTemp.txt");
 			String dom=ut.driver.getPageSource();
 			dom=ut.prettyprintxml(dom);
 			out.println(dom);
 			File f = new File("src/main/java/recursos/dom.txt");
 			if(!f.exists()) { 
 			    out2 = new PrintWriter("src/main/java/recursos/dom.txt");
 				out2.println(dom);
 				out2.close();
 			}
     	 } catch (FileNotFoundException e) {
 			System.err.println("No se pudo crear archivo "+e.getMessage());
 		}finally{
 			out.close();
 			//out2.close();
 		}
    	 List<String> lista =compareDoms("src/main/java/recursos/dom.txt","src/main/java/recursos/domTemp.txt");
			for(int i=0;i<lista.size();i++) {
				
				System.out.println(lista.get(i));
			}
    }
    
    public void realizarAcciones() {
    	try {
	    	MobileElement texto=ut.driver.findElementById("com.example.santiagoastros.cbitdemo:id/editText5");
	    	texto.sendKeys("Saludos");
	    	MobileElement texto2=ut.driver.findElementById("com.example.santiagoastros.cbitdemo:id/editText4");
	    	texto2.sendKeys("Formulario");
	    	ut.screenshot("Formulario lleno");
    	}catch(Exception e) {
    		ut.screenshot("Error objeto");
    		System.err.println("Error realizando acciones, se toma pantallazo "+e.getMessage());
    		
    	}
    }
    
    public List<String> compareDoms(String ruta1, String ruta2) {
    	BufferedReader br=null;
    	BufferedReader br2=null;
    	List<String> lista=null;
    	try  {
    		lista=new LinkedList<String>();
    		br = new BufferedReader(new FileReader(ruta1));
    		br2 = new BufferedReader(new FileReader(ruta2));
    	    String line;
    	    String line2;
    	    int contador=0;

    	    while ((line = br.readLine()) != null && (line2=br2.readLine()) !=null) {
    	    	contador++;
    	    
    	    	if(!line.equals(line2)) {
    	    		//int index=StringUtils.indexOfDifference(line, line2);
    	    		//String subtringDiferenciaOri=line.substring(index-5,index+5);
    	    		//String subtringDiferenciaDest=line2.substring(index-5,index+5);
    	    		lista.add("Se encontró una diferencia en la linea "+contador+"\nDom original: " +line+"\nDom actual: "+line2+"\n---------------------------------");
    	    	}
    	    }
    	    
    	}catch(Exception e) {
    		System.err.println("Error leyendo archivos "+e.getMessage());
    	}finally {
    		try {
				br.close();
			} catch (IOException e) {
				System.err.println("No pude cerrar el primer archivo "+e.getMessage());
			}
    		try {
				br2.close();
			} catch (IOException e) {
				System.err.println("No pude cerrar el segundo archivo "+e.getMessage());
			}
    	}
    	return lista;
    }
}
